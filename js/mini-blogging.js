var jsMiniBlogging = angular.module( 'jsMiniBlogging', [
  'ui.router'
]);

jsMiniBlogging.config( function( $stateProvider, $urlRouterProvider ) {
  $urlRouterProvider.otherwise( '/home' );

  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: 'views/home-view.html',
      controller: 'appCtrl'
    });
});


jsMiniBlogging.controller( 'appCtrl', function appCtrl($scope, $rootScope, localStorage) {
  $scope.thought = '';
  $scope.thoughtsList = localStorage.getData() || [];
  $scope.showError = false;

  $scope.addThought = function() {
    if ($scope.thought) {
      $scope.thoughtsList = localStorage.addElement({
        thought: $scope.thought,
        count: 0,
        votedBy: []
      });
      $scope.thought = '';
      $scope.showError = false;
    } else {
      $scope.showError = true;
    };
  }

  $scope.modifyCount = function (thought, amount, user) {
    var alreadyVoted = false;
    if (thought.count !== 0 || amount !== -1) {
      for (var i = 0; i < thought.votedBy.length; i++) {
        if (thought.votedBy[i].user === user) {
          if (thought.votedBy[i].vote !== amount) {
            thought.votedBy[i].vote = amount;
            thought.count = thought.count + amount;
          };
          alreadyVoted = true;
          break;
        };
      };
      if (!alreadyVoted) {
        thought.count = thought.count + amount;
        thought.votedBy.push({
          user: user,
          vote: amount
        });
      };
      localStorage.setData($scope.thoughtsList);
    };
  }

  $scope.deleteThought = function (thought) {
    $scope.thoughtsList = localStorage.removeElement(thought);
  }
});

jsMiniBlogging.factory("localStorage", function($window, $rootScope) {
  angular.element($window).on('storage', function(event) {
    if (event.key === 'my-storage') {
      $rootScope.$$phase || $rootScope.$apply();
    }
  });
  var setData = function(val) {
    $window.localStorage && $window.localStorage.setItem('my-storage', JSON.stringify(val));
    return this;
  };
  var getData = function() {
    return JSON.parse($window.localStorage && $window.localStorage.getItem('my-storage')) || [];
  };
  var addElement = function(val) {
    var stored = getData();
    stored.unshift(val);
    setData(stored);
    return stored;
  };
  var removeElement = function(val) {
    var stored = getData();
    var index = -1;
    for (var i = 0; i < stored.length; i++) {
      if (stored[i].thought === val) {
        index = i;
        break;
      };
    };
    if (index !== -1) {
      stored.splice(index, 1);
    };
    setData(stored);
    return stored;
  };
  var clear = function() {
    $window.localStorage && $window.localStorage.clear();
  };
  return {
    setData: setData,
    getData: getData,
    addElement: addElement,
    removeElement: removeElement,
    clear: clear
  };
});